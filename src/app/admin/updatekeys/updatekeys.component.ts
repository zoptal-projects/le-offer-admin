import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppConfig } from "../../app.config";
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { UpdateKeysService } from './updatekeys.service';
import { PagesComponent } from '../../pages/pages.component';


declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'updatekeys',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./updatekeys.component.scss'],
    templateUrl: './updatekeys.component.html',
    providers: [UpdateKeysService]
})

export class UpdateKeysComponent {

    keysType = 1;
    cForm: FormGroup;
    mailForm: FormGroup;
    fcmForm: FormGroup;
    twilioForm: FormGroup;
    cData = '';
    mData = '';
    tData = '';
    fData = '';

    constructor(private route: ActivatedRoute, private _service: UpdateKeysService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder, public _isAdmin: PagesComponent) {
        this.cForm = fb.group({
            'cloudName': ['', Validators.required],
            'apiKey': ['', Validators.required],
            'apiSecret': ['', Validators.required],
        });
        this.mailForm = fb.group({
            'domainName': ['', Validators.required],
            'apiKey': ['', Validators.required],
            'fromWho': ['', Validators.required],
        });
        this.fcmForm = fb.group({
            'apiKey': ['', Validators.required],
        });
        this.twilioForm = fb.group({
            'tPhoneNo': ['', Validators.required],
            'AccSid': ['', Validators.required],
            'authToken': ['', Validators.required],
        });
    }
    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'update-keys') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        jQuery(".btn-rounded").hide();
                    }
                }
            }
        }

        this.getPage();
        this.updateKeyType();
    }
    getPage() {
        this._service.getKeys(this.keysType).subscribe(
            res => {
                this.cData = res.data[0].data;
            }
        )
    }
    updateKeyType() {
        this.keysType = 1;
        this._service.getKeys(this.keysType).subscribe(
            res => {
                this.cData = res.data[0].data;
            }
        )

        this.keysType = 2;
        this._service.getKeys(this.keysType).subscribe(
            res => {
                this.mData = res.data[0].data;
            }
        )

        this.keysType = 3;
        this._service.getKeys(this.keysType).subscribe(
            res => {
                this.tData = res.data[0].data;
            }
        )

        this.keysType = 4;
        this._service.getKeys(this.keysType).subscribe(
            res => {
                this.fData = res.data[0].data;
            }
        )

    }
    cloudFrom(data) {
        data._value.type = 1;
        // console.log('data', data._value);
        this._service.addkeys(data._value).subscribe(
            res => {
                // console.log("res", res);
                if (res.code == 200) {
                    swal("Success!", "Key Added Successfully!", "success");
                }
            }
        )
    }
    mailgunForm(data) {
        data._value.type = 2;
        this._service.addkeys(data._value).subscribe(
            res => {
                if (res.code == 200) {
                    swal("Success!", "Key Added Successfully!", "success");
                }
            }
        )
    }
    fcmkeyForm(data) {
        data._value.type = 4;
        this._service.addkeys(data._value).subscribe(
            res => {
                if (res.code == 200) {
                    swal("Success!", "Key Added Successfully!", "success");
                }
            }
        )
    }
    twilioData(data) {
        data._value.type = 3;
        this._service.addkeys(data._value).subscribe(
            res => {
                if (res.code == 200) {
                    swal("Success!", "Key Added Successfully!", "success");
                }
            }
        )
    }
}