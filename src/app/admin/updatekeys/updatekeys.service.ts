import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';

@Injectable()
export class UpdateKeysService {
    public Url: string;

    constructor(public http: Http, public _config: Configuration) { }
    getKeys(type) {
        let url = this._config.Server + "keys?type=" + type;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    addkeys(data) {
        let url = this._config.Server + "keys";
        let body = JSON.stringify(data);
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }

}