import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';

@Injectable()
export class FAQPointService {
    public Url: string;
    public headers = new Headers({ 'Content-Type': 'application/json' });
    public options = new RequestOptions({
        headers: this.headers
    });
    constructor(public http: Http, public _config: Configuration) { }
    getFaqPoints(id): Observable<any> {
        let url = this._config.Server + "admin/getPoints";
        return this.http.post(url, { topicId: id }, { headers: this._config.headers }).map(res => res.json());
    }
    gotoDeletePoint(point): Observable<any> {
        let url = this._config.Server + "admin/removePoint?pointId=" + point;
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }


}