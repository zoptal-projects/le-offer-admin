import { Component, OnInit, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { AppConfig } from "../../app.config";
import { FAQPointService } from './faqpoint.service';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { PagesComponent } from '../../pages/pages.component';

declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'faqpoint',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./faqpoint.component.scss'],
    templateUrl: './faqpoint.component.html',
    providers: [FAQPointService]
})

export class FAQPointsComponent implements OnInit {
    public rowsOnPage = 10;
    faq: any;
    sub: any;
    pointsList: any;
    ans: any;
    que: any;
    msg: any = false;
    btnFlag = true;

    constructor(private route: ActivatedRoute, private _faqpointservice: FAQPointService, private router: Router, vcRef: ViewContainerRef, public _isAdmin: PagesComponent) {

    }
    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'config') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        jQuery(".buttonCus").hide();jQuery(".thAction").remove();
                        jQuery(".thTitle").css("width","19%");
                        this.btnFlag = false;
                    } else if (roleDt[x] == 110) {
                        this.btnFlag = false;jQuery(".thAction").remove();
                        jQuery(".thTitle").css("width","19%");
                    }
                }
            }
        }

        this.sub = this.route.params.subscribe(params => {
            this.faq = params['id'];
        });
        // console.log("faq",this.faq);
        this._faqpointservice.getFaqPoints(this.faq).subscribe(
            result => {
                // console.log("result", result);
                if (result.data[0].points && result.data[0].points.length > 0) {
                    this.msg = false;
                    this.pointsList = result.data[0].points;
                    // console.log("result", result);
                } else {
                    this.pointsList = [];
                    this.msg = "No Points Available";
                }
            }
        )

    }
    gotoViewPointDetail(point) {
        // console.log("point", point);
        this.que = point.ques;
        this.ans = point.ans;
    }
    addPoints() {
        this.router.navigate(['/app/faq/topics/points', 'add', this.faq]);
    }
    editPoints(id) {
        // console.log(id);
        this.router.navigate(['/app/faq/topics/points', 'edit', id]);
    }
    gotoDeletePoints(point) {
        // console.log(point);
        var top = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Point!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    top.deletePoints(point);
                    swal({
                        title: 'Delete!',
                        text: 'Point Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your Point is safe :)", "error");
                }
            });
    }
    deletePoints(point) {
        this._faqpointservice.gotoDeletePoint(point).subscribe(
            result => {
                // console.log("result", result);
                this.ngOnInit();
            }
        )
    }
}