import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';

@Injectable()
export class AppVersionService {
    public Url: string;

    constructor(public http: Http, public _config: Configuration) { }
    saveAppVersion(data) {
        let url = this._config.Server + 'saveAppVersion';
        let body = JSON.stringify(data);
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    getAllVersion(type) {
        let url = this._config.Server + "appVersion?type=" + type;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    getAllUsers(type, version) {
        let url = this._config.Server + "userDeviceVersion?type=" + type + "&version=" + version;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

}