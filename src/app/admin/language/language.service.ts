import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';
import { timeInterval } from 'rxjs/operator/timeInterval';

@Injectable()
export class LanguageService {


    constructor(public http: Http, public _config: Configuration) { }

    getLangaugeData() {
        let url = this._config.Server + 'language';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    addLanguage(val) {
        let url = this._config.Server + 'language';
        return this.http.post(url, JSON.stringify(val), { headers: this._config.headers }).map(res => res.json());
    }
    gotoEditLanguage(val) {
        let url = this._config.Server + 'language';
        return this.http.put(url, JSON.stringify(val), { headers: this._config.headers }).map(res => res.json());
    }
    deleteLanguage(LangId) {
        let url = this._config.Server + 'language/'+"?langId="+LangId 
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }

}