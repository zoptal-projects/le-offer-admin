import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';
@Injectable()
export class FaqAddPointService {

    public Url: string;
    constructor(public http: Http, public _config: Configuration) { }

    getFileContent(faqId): Observable<any> {
        let url = this._config.Server + "admin/points/" + faqId;
        let body = {};
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }

    gotoSubmitFile(faqId, que, ans): Observable<any> {
        let url = this._config.Server + "admin/addPoints";
        let body = { topicId: faqId, question: que, answer: ans };
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    gotoEditFile(id, que, ans): Observable<any> {
        let url = this._config.Server + "admin/addPoints";
        let body = { topicId: id, question: que, answer: ans };
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    gotoimageUpload(dataURI): Observable<any> {
        let url = this._config.Server + "fileUpload/binary";
        let body = { data: dataURI };
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    getImages(): Observable<any> {
        let url = this._config.Server + "getFiles";
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
}
