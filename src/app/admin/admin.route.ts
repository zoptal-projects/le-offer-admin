//importing all the essential modules
import { AuthGuard } from '../_guards/auth.guard';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from '../pages/pages.component';
import { AdminConfigComponent } from '../admin/adminconfig/adminconfig.component';
import { AdminPolicyComponent } from '../admin/adminpolicy/adminpolicy.component';
import { AdminFAQComponent } from '../admin/faq/adminfaq.component';
import { AdminNewsComponent } from '../admin/adminnews/adminnews.component';
import { FAQTopicsComponent } from './faqtopics/faqtopics.component';
import { AdminAboutComponent } from './adminabout/adminabout.component';
import { AdminStoryComponent } from './adminstory/adminstory.component';
import { FAQPointsComponent } from './faqpoint/faqpoint.component';
import { FaqAddPointComponent } from './faqaddpoint/faqaddpoint.component';
import { SocialMediaComponent } from './socialmedia/socialmedia.component';
import { HashTagComponent } from './hashtag/hashtag.component';
import { HashTagPostComponent } from './hashtag/hashtagpost/hashtagpost.component';
import { UpdateKeysComponent } from './updatekeys/updatekeys.component';
import { PushMarketingComponent } from './pushMarketing/pushMarketing.component';
import { TargeterduserComponent } from './pushMarketing/targetedUser/targetedUser.component';
import { CampaignViewComponent } from './pushMarketing/campaignView/campaignView.component';
import { CampaignClickComponent } from './pushMarketing/campaignClick/campaignClick.component';
import { ManageAccessComponent } from './manageAccess/manageAccess.component';
import { NewCampaignComponent } from './newCampaign/newCampaign.component';
import { SendPushComponent } from './sendPush/sendPush.component';
import { newPushComponent } from './sendPush/newPush/newPush.component';
import { addRolesComponent } from './manageAccess/addRoles/addRoles.component';
import { pushTargetedUserComponent } from './sendPush/targetedUser/targetedUser.component';
import { AppVersionComponent } from './appVersion/appVersion.component';
import { usersAppVersionComponent } from './appVersion/usersAppVersion/usersAppVersion.component';
import { FAQComponent } from './adminfaq/adminfaq.component';
import { LanguageComponent } from './language/language.component';
import { FilterComponent } from './filter/filter.component';
import { MessageComponent } from './message/message.component';


//=============== routing for user modules ======================
//=============== exporting the route to the main app =============
export const AdminRoutes: Routes = [

  {
    path: 'app',
    component: PagesComponent,
    children: [
      {
        path: 'terms',
        component: AdminConfigComponent,
        canActivate: [AuthGuard],
        pathMatch: 'full',
        data: {
          title: 'Terms and Condition'
        }
      },
      {
        path: 'policy',
        component: AdminPolicyComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Policy'
        }
      },
      // {
      //   path: 'faq',
      //   component: AdminFAQComponent,
      //   canActivate: [AuthGuard],
      //   data: {
      //     title: 'FAQ'
      //   }
      // },
      {
        path: 'help',
        component: FAQComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Help'
        }
      },
      // {
      //   path: 'about',
      //   component: AdminAboutComponent,
      //   //  canActivate: [AuthGuard],
      // },
      {
        path: 'news',
        component: AdminNewsComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'News'
        }
      },
      {
        path: 'faq/topics/:faq',
        component: FAQTopicsComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'FAQ Topics'
        }
      },
      // {
      //   path: 'faq/topics',
      //   component: FAQTopicsComponent,
      //   //  canActivate: [AuthGuard],
      // },
      {
        path: 'faq/topics/points/:id',
        component: FAQPointsComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'FAQ Points'
        }
      },
      // {
      //   path: 'faq/topics/points/addpoint/:faq',
      //   component: FaqAddPointComponent,
      //   //  canActivate: [AuthGuard],
      // },
      // {
      //   path: 'faq/topics/points/editpoint/:faq',
      //   component: FaqAddPointComponent,
      //   //  canActivate: [AuthGuard],
      // },
      {
        path: 'faq/topics/points/:type/:faq',
        component: FaqAddPointComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'ourstory',
        component: AdminStoryComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Our Story'
        }
      },
      {
        path: 'social-media',
        component: SocialMediaComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'hash-tag',
        component: HashTagComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Hash Tag List'
        }
      },
      {
        path: 'hash-tag/postby-hashtag/:name',
        component: HashTagPostComponent,
        canActivate: [AuthGuard],

      },
      {
        path: 'update-keys',
        component: UpdateKeysComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Update Keys'
        }
      },
      {
        path: 'campaign',
        component: PushMarketingComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Push Marketing'
        }
      },
      {
        path: 'campaign/targeted-user/:id',
        component: TargeterduserComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Targeted User'
        }
      },
      {
        path: 'campaign/view/:id',
        component: CampaignViewComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'View'
        }
      },
      {
        path: 'campaign/click/:id',
        component: CampaignClickComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Click'
        }
      },
      {
        path: 'campaign/launch-new-campaign',
        component: NewCampaignComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'New Campaign'
        }
      },
      {
        path: 'manage-access',
        component: ManageAccessComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Manage Access'
        }
      },
      {
        path: 'manage-access/add-roles',
        component: addRolesComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Add Roles'
        }
      },
      {
        path: 'push-notification',
        component: SendPushComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Send Push'
        }
      },
      {
        path: 'push-notification/send-new-notification',
        component: newPushComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Send New Push'
        }
      },
      {
        path: 'push-notification/targeted-users/:id',
        component: pushTargetedUserComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Targeted Users'
        }
      },
      {
        path: 'app-version',
        component: AppVersionComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'App Verion'
        }
      },
      {
        path: 'app-version/users/:type/:version',
        component: usersAppVersionComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Users'
        }
      },
      {
        path: 'language',
        component: LanguageComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Language'
        }
      },
      {
        path: 'filter',
        component: FilterComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Filter'
        }
      },
      {
        path: 'message',
        component: MessageComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Message'
        }
      }
    ]
  },

];


