import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';

@Injectable()
export class AdminFAQService {
    public Url: string;
    public headers = new Headers({ 'Content-Type': 'application/json' });
    public options = new RequestOptions({
        headers: this.headers
    });
    constructor(public http: Http, public _config: Configuration) { }

    getAdminFaq(): Observable<any> {
        let url = this._config.Server + "faq/getCategory";
        return this.http.post(url, {}, { headers: this._config.headers }).map(res => res.json());
    }
    gotoAddcategory(data): Observable<any> {
        let url = this._config.Server + "faq/addCategory";
        let body = JSON.stringify(data);
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    gotoEditFaq(data): Observable<any> {
        // console.log("data",data);
        let url = this._config.Server + "faq/addCategory";
        let body = JSON.stringify(data);
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    gotoDeteleCat(id): Observable<any> {
        let url = this._config.Server + "faq/deleteCategory";
        let body = { faqCategoryId: id };
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }

}