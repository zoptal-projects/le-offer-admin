import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { AppConfig } from "../../app.config";
import { AdminFAQService } from './adminfaq.service';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { PagesComponent } from '../../pages/pages.component';

declare var swal: any;
declare var sweetAlert: any;
declare var CLOUDNAME: string, CLOUDPRESET: string;

@Component({
    selector: 'adminfaq',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./adminfaq.component.scss'],
    templateUrl: './adminfaq.component.html',
    providers: [AdminFAQService]
})

export class AdminFAQComponent {
    public rowsOnPage = 10;
    faqData: any;
    catId: any;
    public obj = '';
    icon: any;
    addCategory: FormGroup;
    editCategory: FormGroup;
    state = 1;
    eFaqData: any;
    data: any;
    public url: any;
    errorImg = false;
    public msg: any = false;
    btnFlag = true;

    uploader: CloudinaryUploader = new CloudinaryUploader(
        new CloudinaryOptions({ cloudName: CLOUDNAME, uploadPreset: CLOUDPRESET })
    );
    imageUploader = (item: any, response: string, status: number, headers: any) => {
        // alert(2);
        let cloudinaryImage = JSON.parse(response);
        this.url = cloudinaryImage.secure_url;
        // console.log("image url", this.url);
        // if (this.state == 1) {
        //     this.submitData(this.url);
        // } else if (this.state == 0) {
        //     this.editFaqData(this.url);
        // }
        return { item, response, status, headers };
    };



    constructor(private _appConfig: AppConfig, private _adminfaqservice: AdminFAQService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder, public _isAdmin: PagesComponent) {
        this.addCategory = fb.group({
            'faqCategory': ['', Validators.required],
            'icon': "",
        });
        this.editCategory = fb.group({
            'faqCategory': ['', Validators.required],
            'icon': "",
        })
    }
    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'config') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        this.btnFlag = false; jQuery(".thAction").remove();
                        jQuery(".thTitle").css("width", "28%");
                        jQuery(".buttonCus").hide();
                    } else if (roleDt[x] == 110) {
                        this.btnFlag = false; jQuery(".thAction").remove();
                        jQuery(".thTitle").css("width", "28%");
                    }
                }
            }
        }

        this.uploader.onSuccessItem = this.imageUploader;
        this._adminfaqservice.getAdminFaq().subscribe(
            result => {
                this.faqData = result.data;
                // console.log("result", result);
            }
        )
    }
    upload() {
        // let photo = jQuery("#photo").val();
        // if (photo) {
        //     // alert(1);
        //     this.errorImg = false;
        this.uploader.uploadAll();
        // } else {
        //     this.errorImg = true;
        // }
        // console.log(photo);

    }
    submitForm(value) {
        this.data = value._value;
        this.data.faqCatIcon = this.url;
        if (this.url.length > 0) {
            this.msg = false;
            this._adminfaqservice.gotoAddcategory(this.data).subscribe(
                result => {
                    if (result.code == 200) {
                        swal("Success!", "Title Added Successfully!", "success");
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }

                    jQuery('#addFaqCategory').modal('hide');
                    this.ngOnInit();
                    this.addCategory.reset();
                }
            )
        } else {
            this.msg = true;
        }

    }
    submitData(url) {
        this.data.faqCatIcon = url;
        // console.log("data", this.data);
        this._adminfaqservice.gotoAddcategory(this.data).subscribe(
            result => {
                // console.log("result", result);
                if (result.code == 200) {
                    swal("Good job!", "Title Added Successfully!", "success");
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }

                jQuery('#addFaqCategory').modal('hide');
                this.ngOnInit();
                this.addCategory.reset();
            }
        )
    }
    gotoEditCategory(faq) {
        // console.log("faq", faq);
        this.catId = faq._id;
        this.obj = faq;
        this.url = faq.faqCatIcon;

    }
    editForm(value) {
        // console.log("lo", value._value);
        value._value.faqCategoryId = this.catId;
        this.eFaqData = value._value;
        this.eFaqData.faqCatIcon = this.url;
        this._adminfaqservice.gotoEditFaq(this.eFaqData).subscribe(
            result => {
                // console.log("result", result);
                if (result.code == 200) {
                    swal("Success!", "Title Edited Successfully!", "success");
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }

                jQuery('#editCat').modal('hide');
                this.ngOnInit();
                this.addCategory.reset();
            }
        )
    }

    gotoTopics(faq) {
        this.router.navigate(['/app/faq/topics', faq]);
    }
    gotoDeleteCategory(id) {
        var cat = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Faq!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    cat.deleteCat(id);
                    swal({
                        title: 'Delete!',
                        text: 'Faq Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your Faq is safe :)", "error");
                }
            });
    }
    deleteCat(id) {
        this._adminfaqservice.gotoDeteleCat(id).subscribe(
            result => {
                // console.log("result", result);
                this.ngOnInit();

            }
        )
    }
}