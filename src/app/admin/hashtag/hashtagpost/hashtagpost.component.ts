import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { AppConfig } from "../../../app.config";
import { Router, ActivatedRoute } from '@angular/router';
import { HashTagPostService } from './hashtagpost.service';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { PagesComponent } from '../../../pages/pages.component';

@Component({
    selector: 'hashtagpost',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./hashtagpost.component.scss'],
    templateUrl: './hashtagpost.component.html',
    providers: [HashTagPostService]
})

export class HashTagPostComponent {
    sub: any;
    name: any;
    public rowsOnPage = 10;
    p = 0;
    public searchEnabled = 0;
    public searchTerm = '';
    msg = false;
    pHashData = [];
    detailData: any;

    constructor(private route: ActivatedRoute, private _appConfig: AppConfig, private _service: HashTagPostService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder, public _isAdmin: PagesComponent) {

    }
    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'hash-tag') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } 
                }
            }
        }

        this.sub = this.route.params.subscribe(params => {
            this.name = params['name'];
        });
        this.getPage();
    }
    getPage() {
        this._service.getAllHashtagPost(this.name, this.p, this.rowsOnPage, this.searchEnabled, this.searchTerm).subscribe(
            res => {
                // console.log("res", res);
                if (res.response && res.response.length > 0) {
                    if (res.response.length < 10) {
                        jQuery("#loadButton").hide();
                    } else {
                        jQuery("#loadButton").show();
                    }
                    this.msg = false;
                    res.response.forEach(element => {
                        this.pHashData.push(element);
                    });
                    this.p = this.p + this.rowsOnPage;
                } else {
                    this.msg = true;
                    this.pHashData = [];
                }
            }
        )
    }

    getPageOnSearch(term) {
        this.searchTerm = term;
        if (this.searchTerm.length > 0) {
            this.searchEnabled = 1;
        } else {
            this.searchEnabled = 0;
        }
        this.p = 0;
        this.pHashData = [];
        this.getPage();
    }
    gotoUserDatail(user) {
        // console.log("user",user)
        this._service.getUserDetail(user).subscribe(
            result => {
                this.detailData = result.data;
                // console.log("result", result);
            }
        )
    }
}