import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppConfig } from "../../app.config";
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { HashTagService } from './hashtag.service';
import { PagesComponent } from '../../pages/pages.component';

declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'hashtag',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./hashtag.component.scss'],
    templateUrl: './hashtag.component.html',
    providers: [HashTagService]
})

export class HashTagComponent {
    public rowsOnPage = 10;
    public p = 0;
    msg = false;
    public searchEnabled = 0;
    public searchTerm = '';
    hashData = [];
    constructor(private route: ActivatedRoute, private _service: HashTagService, private router: Router, vcRef: ViewContainerRef, public _isAdmin: PagesComponent) { }
    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'hash-tag') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    }
                }
            }
        }

        this.getPage();
    }
    getPage() {
        this._service.getAllHashTag(this.rowsOnPage, this.p, this.searchEnabled, this.searchTerm).subscribe(
            res => {
                if (res.response && res.response.length > 0) {
                    if (res.response.length < 10) {
                        jQuery("#loadButton").hide();
                    } else {
                        jQuery("#loadButton").show();
                    }
                    this.msg = false;
                    res.response.forEach(element => {
                        this.hashData.push(element);
                    });
                    this.p = this.p + this.rowsOnPage;
                } else {
                    this.msg = true;
                    this.hashData = [];
                }
            }
        )
    }

    getPageOnSearch(term) {
        this.searchTerm = term;
        if (this.searchTerm.length > 0) {
            this.searchEnabled = 1;
        } else {
            this.searchEnabled = 0;
        }
        this.p = 0;
        this.hashData = [];
        this.getPage();
    }
    gotoHashTag(hashtag) {
        this.router.navigate(['/app/hash-tag/postby-hashtag', hashtag]);
    }


}