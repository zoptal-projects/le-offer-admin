import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';

@Injectable()
export class HashTagService {
    public Url: string;

    constructor(public http: Http, public _config: Configuration) { }

    getAllHashTag(limit, offset, search, term) {
        let url = this._config.Server + "getAllHashTags";
        let body = {
            limit: limit,
            offset: offset,
            search: search,
            term: term
        };
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }

}