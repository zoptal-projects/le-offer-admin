import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';

@Injectable()
export class FAQTopicService {
    public Url: string;
    public headers = new Headers({ 'Content-Type': 'application/json' });
    public options = new RequestOptions({
        headers: this.headers
    });
    constructor(public http: Http, public _config: Configuration) { }

    getTopics(faq): Observable<any> {
        let url = this._config.Server + "admin/getTopics";
        return this.http.post(url, { categoryId: faq }, { headers: this._config.headers }).map(res => res.json());
    }

    gotoAddTopics(data): Observable<any> {
        let url = this._config.Server + "admin/addTopics";
        let body = JSON.stringify(data);
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    subminEditedData(data): Observable<any> {
        let url = this._config.Server + "admin/addTopics";
        let body = JSON.stringify(data);
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    gotoDeletTopics(id): Observable<any> {
        let url = this._config.Server + "admin/addTopics?topicsId=" + id;
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }


}