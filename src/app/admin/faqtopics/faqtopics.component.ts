import { Component, OnInit, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { AppConfig } from "../../app.config";
import { FAQTopicService } from './faqtopics.service';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { PagesComponent } from '../../pages/pages.component';

declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'faqtopics',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./faqtopics.component.scss'],
    templateUrl: './faqtopics.component.html',
    providers: [FAQTopicService]
})

export class FAQTopicsComponent implements OnInit {
    public rowsOnPage = 10;
    addTopics: FormGroup;
    editTopics: FormGroup;
    faq: any;
    sub: any;
    obj: any = '';
    topicId: any;
    topicsList: any = [];
    msg: any = false;
    btnFlag = true;
    
    constructor(private route: ActivatedRoute, private _faqtopicservice: FAQTopicService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder,public _isAdmin: PagesComponent) {
        this.addTopics = fb.group({
            'name': ['', Validators.required],
        });
        this.editTopics = fb.group({
            'name': ['', Validators.required],
        })
    }
    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'config') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        jQuery(".buttonCus").hide();
                        this.btnFlag = false;    jQuery(".thAction").remove();
                        jQuery(".thTitle").css("width","38%");                    
                    } else if (roleDt[x] == 110) {
                        this.btnFlag = false;    jQuery(".thAction").remove();
                        jQuery(".thTitle").css("width","38%");                    
                    }
                }
            }
        }

        this.sub = this.route.params.subscribe(params => {
            this.faq = params['faq'];
        });
        // console.log("faq", this.faq);
        this._faqtopicservice.getTopics(this.faq).subscribe(
            result => {
                // console.log("result", result);
                if (result.data && result.data.length > 0) {
                    this.msg = false;
                    this.topicsList = result.data;
                    // console.log("result", result);
                } else {
                    this.topicsList = [];
                    this.msg = "No Topics Available";
                }
            }
        )
    }
    submitForm(value) {
        value._value.faqCategory = this.faq;
        this._faqtopicservice.gotoAddTopics(value._value).subscribe(
            result => {
                // console.log("result", result);
                if (result.code == 200) {
                    swal("Good job!", "Topics Added Successfully!", "success");
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
                jQuery('#addFaqTopics').modal('hide');
                this.ngOnInit();
                this.addTopics.reset();
            }
        )
    }
    gotoFaqTopics(data) {
        this.obj = data;
        // console.log("data", this.obj);
        this.topicId = data._id;
    }
    editedDataForm(value) {
        value._value.topicsId = this.topicId;
        this._faqtopicservice.subminEditedData(value._value).subscribe(
            result => {
                // console.log("result", result);
                if (result.code == 200) {
                    swal("Good job!", "Topics Edited Successfully!", "success");
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
                jQuery('#editFaqTopics').modal('hide');
                this.ngOnInit();
                this.editTopics.reset();
            }
        )
    }
    gotoDeleteTopics(id) {
        var top = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Topics!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    top.deleteTopics(id);
                    swal({
                        title: 'Delete!',
                        text: 'Topics Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your Topics is safe :)", "error");
                }
            });
    }
    deleteTopics(id) {
        this._faqtopicservice.gotoDeletTopics(id).subscribe(
            result => {
                // console.log("result", result);
                this.ngOnInit();
            }
        )
    }
    gotoFaqPoint(id) {
        this.router.navigate(['/app/faq/topics/points', id]);
    }
}